/* global BigInt */
import React, { useEffect, useCallback, useState } from "react";
import { useWalletConnectorSelector } from "@concordium/react-components";
import { HttpProvider, JsonRpcClient } from "@concordium/web-sdk";
import { JSON_RPC_URL } from "./settingConcordium";
import { createChallenge, microCcdToCcd } from "./services/concordium.service";
const CONNECTION_TYPE = "BrowserWallet"; // "WalletConnect";;
const RPC = new JsonRpcClient(new HttpProvider(JSON_RPC_URL));

type WalletConcordiumProps = {
  actionWallet: string;
  walletProps: any;
};

export const WalletConcordium = ({
  actionWallet,
  walletProps,
}: WalletConcordiumProps) => {
  const [balance, setBalance] = useState<string>("");
  const [walletAddress, setWalletAddress] = useState<string>("");
  const { isSelected, isConnected, select } = useWalletConnectorSelector(
    CONNECTION_TYPE,
    walletProps
  );

  const connect = useCallback(async () => {
    console.log("isSelected", isSelected);
    if (isSelected) {
      // console.log("toconnect", walletProps);
      // const account = await walletProps.activeConnector.client.getMostRecentlySelectedAccount();
      // console.log("account", account);
      // if (account) {
      //   return dispatch(setWalletState(true, false, account, 0));
      // }
      console.log("connect");
      await walletProps.connectActive();
    }
  }, [isSelected]);

  const disconnect = useCallback(async () => {
    if (isConnected) {
      console.log("disconnecting");
      await walletProps.disconnectActive();
      setBalance("");
    }
  }, [isConnected]);

  const addChallenge = useCallback(async () => {
    console.log("wallet props", walletProps);
    // test with blockchain
    const challenge = {
      status: "pending",
      _id: "63d6f82b8f37aa477e567c49",
      isPublic: true,
      name: "Test Concordium 2",
      startDate: "2023-02-01T03:56:00.000Z",
      game: { _id: "63c82dacade43c82776b809f", gameName: "One Tap" },
      description: "<p>tt</p>",
      amount: 2,
      platform: "nothing",
      maxPlayersInPublicChallenge: 2,
      creator: "63b348484dabbe7aca9dbc4e",
      createdAt: "2023-01-29T22:50:19.638Z",
      updatedAt: "2023-01-29T22:50:19.638Z",
      __v: 0,
    };
    const isMobile = false;
    const session = isMobile ? walletProps.connectionActive.session : null;
    const rpcClient = isMobile
      ? walletProps.connectionActive.connector.client
      : walletProps.activeConnection.client;
    console.log(
      "hash of transaction",
      await createChallenge(walletAddress, rpcClient, challenge)
    );
  }, [walletProps]);

  useEffect(() => {
    if (!isSelected) {
      select();
    }
  }, []);

  useEffect(() => {
    console.log("props", walletProps);
    if (isConnected) {
      const { activeConnection } = walletProps;
      // GET BALANCE
      const {
        activeConnection: { client },
      } = walletProps;
      client.jsonRpcClient
        .getAccountInfo(walletProps.activeConnectedAccount)
        .then(
          (accountInfo: {
            accountAmount: string | number | bigint | boolean;
          }) => {
            setWalletAddress(walletProps.activeConnectedAccount);
            const balance = microCcdToCcd(BigInt(accountInfo.accountAmount));
            setBalance(balance);
          },
          console.error
        );
    }
  }, [isConnected, walletProps]);
  // useEffect(() => {
  //   if (actionWallet === "concordium:connect" && !isConnected) {
  //     connect();
  //   }
  //   if (isConnected) {
  //     if (actionWallet === "concordium:disconnect" && isConnected) {
  //       disconnect();
  //     }
  //   }
  // }, [actionWallet]);
  return (
    <div>
      <img
        style={{
          cursor: "pointer",
          border: isConnected ? "1px solid green" : "none",
          opacity: isConnected ? 1 : 0.8,
        }}
        height={50}
        alt="BrowserWalletConcordium"
        src={
          "https://concordium.com/wp-content/uploads/2022/08/Group-1000003277.png"
        }
        onClick={() => {
          if (isConnected) {
            disconnect();
          } else {
            connect();
          }
        }}
      />
      {isConnected ? (
        <div>
          {balance}
          <button onClick={addChallenge}>Create Challenge</button>
        </div>
      ) : (
        <div>Not connected</div>
      )}
    </div>
  );
};

export default WalletConcordium;

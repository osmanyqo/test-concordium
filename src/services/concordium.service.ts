/* global BigInt */
import {
  toBuffer,
  CcdAmount,
  AccountTransactionType,
} from "@concordium/web-sdk";
import { HttpProvider, JsonRpcClient } from "@concordium/web-sdk";
import { Buffer } from "buffer";
import {
  CHAIN_ID,
  SCHEMA_JSON,
  RAW_SCHEMA_BASE64,
  MAX_CONTRACT_EXECUTION_ENERGY,
  JSON_RPC_URL,
} from "../settingConcordium";
const TESTNET_INDEX_CONTRACT = 2120;
const CONTRACT_INDEX = TESTNET_INDEX_CONTRACT;
const RPC = new JsonRpcClient(new HttpProvider(JSON_RPC_URL));

function ISODateString(d: Date) {
  function pad(n: string | number) {
    return n < 10 ? "0" + n : n;
  }
  return (
    d.getUTCFullYear() +
    "-" +
    pad(d.getUTCMonth() + 1) +
    "-" +
    pad(d.getUTCDate()) +
    "T" +
    pad(d.getUTCHours()) +
    ":" +
    pad(d.getUTCMinutes()) +
    ":" +
    pad(d.getUTCSeconds()) +
    "Z"
  );
}
const methods = {
  acceptChallenge: "ManaChallenge.accept_challenge",
  addChallenge: "ManaChallenge.add_challenge",
  cancelChallenge: "ManaChallenge.cancel_challenge",
  setPercentageFees: "ManaChallenge.set_percentage_fees",
  setWinnerAPI: "ManaChallenge.set_winner_api",
  setWinnerPlayer: "ManaChallenge.set_winner_player",
  setWinnerValidator: "ManaChallenge.set_winner_validator",
  viewChallenge: "ManaChallenge.view_challenge",
  viewPercentagesFees: "ManaChallenge.view_percentage_fees",
  viewValidation: "ManaChallenge.view_validation",
};

// example contract context
// export interface ContractContext {
//     invoker?: ContractAddress | AccountAddress;
//     contract: ContractAddress;
//     amount?: CcdAmount;
//     method: string;
//     parameter?: Buffer;
//     energy?: bigint;
// }

export async function getContractInfo() {
  const info = await RPC.getInstanceInfo({
    index: BigInt(CONTRACT_INDEX),
    subindex: BigInt(0),
  });
  if (!info) {
    throw new Error(`contract ${CONTRACT_INDEX} not found`);
  }
  return info;
}

export async function createChallengeWithRPC(
  walletAddress: any,
  challenge: { _id: any; amount: number; expiration: any },
  game: { name: any }
) {
  const payload = {
    challenge_id: challenge._id,
    challenge: {
      amount: (10000000 * challenge.amount).toString(),
      game: game.name,
      opponents: {
        Some: [],
      },
      challenger: walletAddress,
      expiration: ISODateString(challenge.expiration),
      is_public: true,
      max_opponents: {
        None: [],
      },
    },
  };
  const method = `add_challenge`;
  const result = await RPC.invokeContract({
    contract: { index: BigInt(CONTRACT_INDEX), subindex: BigInt(0) },
    method,
  });
  if (!result) {
    throw new Error(
      `invocation of method "${method}" on contract "${CONTRACT_INDEX}" returned no result`
    );
  }
}

export async function getFees(payload: any) {
  const method = methods.viewPercentagesFees;
  const result = await RPC.invokeContract({
    contract: { index: BigInt(CONTRACT_INDEX), subindex: BigInt(0) },
    method,
    parameter: payload ? (payloadToJson(payload) as unknown as any) : null,
  });
  console.log(result);
  if (!result) {
    throw new Error(
      `invocation of method "${method}" on contract "${CONTRACT_INDEX}" returned no result`
    );
  }
  const buffer = toBuffer((result as unknown as any).returnValue || "", "hex");
  const [state] = decodeByte(buffer as any, 0);
  console.log("value returned", state);
  return state;
}

export async function getChallenge(challengeId: string) {
  const payload = {
    challenge_id: challengeId,
  };
  const method = methods.viewChallenge;
  const result = await RPC.invokeContract({
    contract: { index: BigInt(CONTRACT_INDEX), subindex: BigInt(0) },
    method,
    parameter: payloadToJson(payload) as unknown as any,
  });
  console.log(result);
  if (!result) {
    throw new Error(
      `invocation of method "${method}" on contract "${CONTRACT_INDEX}" returned no result`
    );
  }
  const buffer = toBuffer((result as any).returnValue || "", "hex");
  const [state] = decodeByte(buffer as any, 0);
  console.log("value returned", state);
  return state;
}

export async function createChallenge(
  sender: any,
  client: {
    sendTransaction: (
      arg0: any,
      arg1: AccountTransactionType,
      arg2: {
        amount: CcdAmount;
        address: { index: string; subindex: bigint };
        receiveName: string;
        maxContractExecutionEnergy: any;
      },
      arg3: {
        challenge_id: any;
        challenge: {
          amount: string;
          game: any;
          opponents: { None: never[] };
          challenger: any;
          expiration: string;
          is_public: boolean;
          max_opponents: { Some: number[] };
        };
      },
      arg4: any
    ) => any;
  },
  challenge: {
    amount: any;
    _id: any;
    game: { gameName: any };
    startDate: string | number | Date;
    maxPlayersInPublicChallenge: any;
  }
) {
  const method = methods.addChallenge;
  const amount = ccdToMicroCcd(challenge.amount);
  console.log("amount", amount);
  const payload = {
    challenge_id: challenge._id,
    challenge: {
      amount: amount.toString(),
      game: challenge.game.gameName,
      opponents: {
        None: [],
      },
      challenger: sender,
      expiration: ISODateString(new Date(challenge.startDate)),
      is_public: true,
      max_opponents: {
        Some: [Number(challenge.maxPlayersInPublicChallenge)],
      },
    },
  };

  const txHash = await client.sendTransaction(
    sender,
    AccountTransactionType.Update,
    {
      amount: new CcdAmount(amount),
      address: { index: CONTRACT_INDEX.toString(), subindex: BigInt(0) },
      receiveName: method,
      maxContractExecutionEnergy: MAX_CONTRACT_EXECUTION_ENERGY,
    },
    payload,
    SCHEMA_JSON.entrypoints.add_challenge.parameter // RAW_SCHEMA_BASE64,
  );
  console.log(txHash);
  return txHash;
}

// export async function createChallenge(session, sender, client, challenge) {
//     const method = methods.addChallenge;
//     const amount = ccdToMicroCcd(challenge.amount).toString();
//     const payload = {
//         "challenge_id": challenge._id,
//         "challenge": {
//             amount,
//             "game": challenge.game.gameName,
//             "opponents": {
//                 "None": []
//             },
//             "challenger": sender,
//             "expiration": ISODateString(new Date(challenge.startDate)),
//             "is_public": true,
//             "max_opponents": {
//                 "Some": [Number(challenge.maxPlayersInPublicChallenge)]
//             }
//         }
//     };
//     const params = {
//         type: 'Update', // AccountTransactionType.TransferToPublic, // TODO replace with name from Web SDK once it's been updated
//         sender,
//         payload: contractUpdatePayload(amount, method, JSON.stringify(payload)),
//     };
//     console.log(params);
//     try {
//         const transaction = await client.request({
//             topic: session.topic,
//             request: {
//                 method: 'sign_and_send_transaction',
//                 params,
//             },
//             chainId: CHAIN_ID,
//         })
//         return transaction;
//     } catch (e) {
//         console.error(e);
//         throw e;
//     }
// }

export function microCcdToCcdString(amount: number | BigInt) {
  const int = BigInt(amount.toString()) / BigInt(1e6);
  const frac = BigInt(amount.toString()) % BigInt(1e6);
  return `${int}.${frac.toString().padStart(6, "0")}`;
}

export function microCcdToCcd(amount: number | bigint) {
  return Number(microCcdToCcdString(amount)).toFixed(2);
}

export function ethToWei(amount: number) {
  return amount * 1e18;
}

export function ccdToMicroCcd(amount: string) {
  const [int, frac] = parseFloat(amount).toFixed(2).split(".");
  return BigInt(int) * BigInt(1e6) + BigInt(frac.padEnd(6, "0"));
}

export function decodeByte(buffer: Buffer, offset: number) {
  return [buffer.readUInt8(offset), offset + 1];
}

export function payloadToJson(payload: { challenge_id: string }) {
  return JSON.stringify(payload);
  // return toBuffer(JSON.stringify(payload)).toString('hex');
}

export function contractUpdatePayload(
  amount: any,
  method: any,
  parameter = ""
) {
  const payload = {
    amount,
    contractAddress: {
      // DEPRECATED
      index: CONTRACT_INDEX,
      subindex: BigInt(0),
    },
    address: {
      index: CONTRACT_INDEX,
      subindex: BigInt(0),
    },
    receiveName: method,
    maxContractExecutionEnergy: MAX_CONTRACT_EXECUTION_ENERGY,
    message: toBuffer("Creating new challenge"),
    param: toBuffer(parameter).toString("hex"),
  };
  return JSON.stringify(payload, (key, value) => {
    if (value?.type === "Buffer") {
      return toBuffer(value.data).toString("hex");
    }
    if (typeof value === "bigint") {
      return Number(value);
    }
    return value;
  });
}

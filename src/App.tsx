import React from "react";
import {
  WithWalletConnector,
  useWalletConnectorSelector,
  WalletConnectionProps,
} from "@concordium/react-components";
import logo from "./logo.svg";
import "./App.css";
import WalletConcordium from "./WalletConcordium";
import { TESTNET, WALLET_CONNECT_OPTIONS_TESTNET } from "./settingConcordium";

function App() {
  return (
    <WithWalletConnector
      network={TESTNET}
      walletConnectOpts={WALLET_CONNECT_OPTIONS_TESTNET}
    >
      {(props) => <WalletConcordium actionWallet="" walletProps={props} />}
    </WithWalletConnector>
  );
}

export default App;
